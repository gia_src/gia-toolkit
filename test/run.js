require( 'source-map-support' ).install();

require( 'babel/register' )({
	whitelist: [ 'es6.modules' ]
});

require( './spec/spec' );
