import * as assert from 'assert';
import * as toolkit from '../../dist/gia-toolkit.js';

describe( 'gia-toolkit', () => {
	it( 'exists', () => {
		assert.ok( toolkit );
	});
});
