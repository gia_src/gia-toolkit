# gia-toolkit changelog

## 1.2.0

* Remove XDR support
* `get` rejects if status code does not begin with a 2

## 1.1.0

* Generate CommonJS build alongside ES6 build
* Update various dependencies

## 1.0.6-7

* `slugify` now removes diacritics

## 1.0.3-5

* Add bucket function

## 1.0.2

* ???

## 1.0.1

* Include `ui` folder in package

## 1.0.0

* First release
