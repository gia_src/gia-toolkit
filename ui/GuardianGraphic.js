import Ractive from 'ractive';
var __options__ = {
	template: { v: 3, t: [{ p: [1, 1, 0], t: 7, e: "article", a: { "class": "gia-graphic" }, f: [{ p: [2, 2, 31], t: 7, e: "div", a: { "class": "gia-graphic-headerborder" } }, " ", { p: [3, 2, 77], t: 7, e: "h1", a: { "class": "gia-graphic-title" }, f: [{ t: 2, r: "title", p: [3, 32, 107] }] }, " ", { p: [5, 2, 124], t: 7, e: "div", a: { "class": "gia-graphic-contents" }, f: [{ t: 16, p: [6, 3, 161] }] }, " ", { p: [9, 2, 181], t: 7, e: "div", a: { "class": "gia-graphic-credit" }, f: [{ t: 8, r: "credit", p: [10, 3, 216] }] }] }], p: { credit: [{ p: [17, 2, 399], t: 7, e: "p", f: ["Guardian US Interactive"] }] } },
	css: ".gia-graphic-title{font-size:24px;font-weight:700;color:#005689;width:auto;display:inline-block;margin-top:0;margin-bottom:5px;line-height:1}.gia-graphic-headerborder{border-top:4px solid #52c6d8;width:70px;margin-bottom:4px}.gia-graphic-credit{margin:8px 0 0;padding:4px 0 0;border-top:1px solid #000;font-family:'Guardian Text Sans Web';text-transform:uppercase;@include clearfix}.gia-graphic-credit p{margin:0}"
},
    component = {},
    __prop__,
    __export__;

component.exports = {
	data: function data() {
		return {
			title: 'Untitled graphic'
		};
	}
};

if (babelHelpers.typeof(component.exports) === "object") {
	for (__prop__ in component.exports) {
		if (component.exports.hasOwnProperty(__prop__)) {
			__options__[__prop__] = component.exports[__prop__];
		}
	}
}

__export__ = Ractive.extend(__options__);

export default __export__;
//# sourceMappingURL=GuardianGraphic.js.map
