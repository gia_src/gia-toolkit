import Ractive from 'ractive';
import __import0__ from './geometry';
var __options__ = {
	template: { v: 3, t: [{ p: [1, 1, 0], t: 7, e: "div", a: { "class": "map-container" }, f: [{ p: [3, 2, 30], t: 7, e: "div", a: { "class": "gia-key" }, f: [{ p: [4, 3, 54], t: 7, e: "div", a: { "class": "keys" }, f: [{ p: [5, 4, 76], t: 7, e: "div", a: { "class": "sub-keys" }, f: [{ p: [5, 26, 98], t: 7, e: "div", a: { "class": "gia-box key-one" } }, " key one"] }, " ", { p: [6, 4, 151], t: 7, e: "div", a: { "class": "sub-keys" }, f: [{ p: [6, 26, 173], t: 7, e: "div", a: { "class": "gia-box key-two" } }, " key two"] }] }] }, " ", { p: [10, 2, 242], t: 7, e: "div", a: { "class": "map-container" }, f: [{ p: [11, 3, 272], t: 7, e: "svg", a: { viewBox: "0 0 950 620" }, f: [{ t: 4, f: [{ p: [13, 5, 326], t: 7, e: "path", a: { id: [{ t: 2, r: "abbrev", p: [14, 9, 340] }], "class": ["gia-state state-", { t: 2, r: "abbrev", p: [15, 28, 379] }], d: [{ t: 2, r: "path", p: [16, 8, 398] }] } }], n: 52, r: "geometry", p: [12, 4, 303] }] }] }] }] },
	css: ".gia-box,.sub-keys{display:inline-block}.map-container{position:relative;width:100%;height:0;padding:0 0 65.26%}svg{position:absolute;width:100%;height:100%}path{fill:#eee}.gia-state{fill:#e2e2e2;stroke:#fff;stroke-width:3}.gia-key{font-family:'Guardian Text Sans Web',Arial,Helvetica,sans-serif;font-size:14px;margin:0 0 12px;width:100%}.gia-box{width:10px;height:10px;border-radius:30px;-webkit-border-radius:30px;-moz-border-radius:30px}.sub-keys{margin-right:10px}.key-one,.key-two{background-color:#e2e2e2}"
},
    component = {},
    __prop__,
    __export__;
(function () {
	var __dependencies__ = {
		'./geometry': __import0__
	};

	var require = function require(path) {
		if (__dependencies__.hasOwnProperty(path)) {
			return __dependencies__[path];
		}

		throw new Error('Could not find required module "' + path + '"');
	};

	var geometry = require('./geometry'),
	    lookup = {};

	component.exports = {
		data: function data() {
			return {
				geometry: geometry
			};
		}

	};

	if (babelHelpers.typeof(component.exports) === "object") {
		for (__prop__ in component.exports) {
			if (component.exports.hasOwnProperty(__prop__)) {
				__options__[__prop__] = component.exports[__prop__];
			}
		}
	}

	__export__ = Ractive.extend(__options__);
})();

export default __export__;
//# sourceMappingURL=index.js.map
