import Ractive from 'ractive';
import __import0__ from 'toolkit/core/promise';
import __import1__ from 'toolkit/image/loadImage';
var __options__ = {
	template: { v: 3, t: [{ p: [1, 1, 0], t: 7, e: "div", a: { "class": "before-after-container", style: ["padding-bottom: ", { t: 2, r: "padding", p: [1, 60, 59] }, "%;"] }, f: [{ t: 4, f: [], n: 50, r: "loading", p: [2, 2, 76] }, { t: 4, n: 51, f: [{ p: [5, 3, 105], t: 7, e: "div", a: { "class": "before" }, f: [{ p: [6, 4, 129], t: 7, e: "img", a: { src: [{ t: 2, r: "before", s: true, p: [6, 14, 139] }] } }] }, " ", { p: [9, 3, 165], t: 7, e: "div", a: { "class": "after" }, f: [{ p: [10, 4, 188], t: 7, e: "img", a: { src: [{ t: 2, r: "after", s: true, p: [10, 14, 198] }] } }] }], r: "loading" }] }] }
},
    component = {},
    __prop__,
    __export__;
(function () {
	var __dependencies__ = {
		'toolkit/core/promise': __import0__,
		'toolkit/image/loadImage': __import1__
	};

	var require = function require(path) {
		if (__dependencies__.hasOwnProperty(path)) {
			return __dependencies__[path];
		}

		throw new Error('Could not find required module "' + path + '"');
	};

	var Promise = require('toolkit/core/promise');
	var loadImage = require('toolkit/image/loadImage');

	component.exports = {
		data: function data() {
			return {
				loading: true,
				dimensions: [1600, 900]
			};
		},

		computed: {
			padding: function padding() {
				var _get = this.get('dimensions');

				var _get2 = babelHelpers.slicedToArray(_get, 2);

				var width = _get2[0];
				var height = _get2[1];

				return 100 * (height / width);
			}
		},

		oninit: function oninit() {
			var _this = this;

			Promise.all([loadImage(this.get('before')), loadImage(this.get('after'))]).then(function (_ref) {
				var _ref2 = babelHelpers.slicedToArray(_ref, 2);

				var before = _ref2[0];
				var after = _ref2[1];

				_this.before = before;
				_this.after = after;

				if (before.width !== after.width || before.height !== after.height) {
					throw new Error('Before and after images must have the same dimensions (' + before.width + 'x' + before.height + ' vs ' + after.width + 'x' + after.height + ')');
				}

				_this.set('loading', false);
			});
		}
	};

	if (babelHelpers.typeof(component.exports) === "object") {
		for (__prop__ in component.exports) {
			if (component.exports.hasOwnProperty(__prop__)) {
				__options__[__prop__] = component.exports[__prop__];
			}
		}
	}

	__export__ = Ractive.extend(__options__);
})();

export default __export__;
//# sourceMappingURL=index.js.map
