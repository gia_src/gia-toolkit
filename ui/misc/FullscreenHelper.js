import Ractive from 'ractive';
import __import0__ from 'toolkit/dom/fullscreen';
import __import1__ from 'ractive-events-tap';
var __options__ = {
	template: { v: 3, t: [{ p: [1, 1, 0], t: 7, e: "div", a: { "class": ["fullscreen-helper ", { t: 2, x: { r: ["active"], s: "_0?\"fullscreen-helper-active\":\"\"" }, p: [1, 31, 30] }] }, f: [{ t: 4, f: [{ p: [3, 3, 96], t: 7, e: "button", a: { "class": "fullscreen-helper-toggle" }, v: { tap: { m: "toggleFullscreen", a: { r: [], s: "[]" } } }, f: ["toggle fullscreen"] }], n: 50, r: "enabled", p: [2, 2, 78] }, " ", { p: [6, 2, 203], t: 7, e: "div", a: { "class": "fullscreen-helper-content" }, f: [{ t: 16, p: [7, 3, 245] }] }] }] },
	css: ".fullscreen-helper-active{position:fixed;left:0;top:0;right:0;bottom:0;padding:1rem;overflow:auto;background-color:#fff}.fullscreen-helper-toggle{position:absolute;top:1rem;right:1rem;font-family:'Guardian Text Sans Web';background-color:transparent;border:none;color:#999;-webkit-transition:color .2s;transition:color .2s;outline:0}.fullscreen-helper-toggle:hover{color:#666}.fullscreen-helper-toggle:active{outline:0}"
},
    component = {},
    __prop__,
    __export__;
(function () {
	var __dependencies__ = {
		'toolkit/dom/fullscreen': __import0__,
		'ractive-events-tap': __import1__
	};

	var require = function require(path) {
		if (__dependencies__.hasOwnProperty(path)) {
			return __dependencies__[path];
		}

		throw new Error('Could not find required module "' + path + '"');
	};

	var fullscreen = require('toolkit/dom/fullscreen');

	var isIos = !fullscreen.enabled;

	component.exports = {
		data: function data() {
			return {
				enabled: fullscreen.enabled
			};
		},

		onrender: function onrender() {
			var _this = this;

			this.container = this.find('.fullscreen-helper');

			var onchange = function onchange() {
				return _this.set('active', fullscreen.element === _this.container);
			};

			if (fullscreen.enabled) {
				document.addEventListener(fullscreen.raw.fullscreenchange, onchange, false);
				this.on('unrender', function () {
					return document.removeEventListener(fullscreen.raw.fullscreenchange, onchange, false);
				});
			}
		},
		toggleFullscreen: function toggleFullscreen() {
			if (fullscreen.enabled) {
				fullscreen.toggle(this.container);
			}
		},

		events: {
			tap: require('ractive-events-tap')
		}
	};

	if (babelHelpers.typeof(component.exports) === "object") {
		for (__prop__ in component.exports) {
			if (component.exports.hasOwnProperty(__prop__)) {
				__options__[__prop__] = component.exports[__prop__];
			}
		}
	}

	__export__ = Ractive.extend(__options__);
})();

export default __export__;
//# sourceMappingURL=FullscreenHelper.js.map
