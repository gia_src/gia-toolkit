import Ractive from 'ractive';
import __import0__ from 'toolkit/ajax/get';
var __options__ = {
	template: { v: 3, t: [{ p: [1, 1, 0], t: 7, e: "div", a: { "class": "timeline-container" }, f: [{ t: 2, x: { r: ["log", "timelineData"], s: "_0(\"this\",_1)" }, p: [3, 2, 35] }, " ", { p: [5, 2, 71], t: 7, e: "div", a: { "class": "timeline-content" }, f: [{ t: 4, f: [{ p: [8, 3, 146], t: 7, e: "div", a: { "class": "event" }, f: [{ p: [9, 4, 170], t: 7, e: "div", a: { "class": "date" }, f: [{ t: 4, f: [{ p: [9, 34, 200], t: 7, e: "span", a: { "class": "year" }, f: [{ t: 2, r: "year", p: [9, 53, 219] }] }, " ", { p: [9, 71, 237], t: 7, e: "br" }], n: 50, r: "year", p: [9, 22, 188] }, " ", { t: 2, r: "day", p: [9, 83, 249] }, " ", { t: 2, r: "month", p: [9, 93, 259] }] }, " ", { p: [10, 4, 280], t: 7, e: "div", a: { "class": "content" }, f: [{ t: 4, f: [{ p: [12, 6, 334], t: 7, e: "div", a: { "class": "headline" }, f: [{ t: 2, r: "eventheadline", p: [12, 28, 356] }] }], n: 50, r: "eventheadline", p: [11, 5, 306] }, " ", { t: 4, f: [{ p: [16, 5, 421], t: 7, e: "div", a: { "class": "photo" }, f: [{ p: [17, 6, 446], t: 7, e: "img", a: { src: [{ t: 2, r: "photourl", p: [17, 16, 456] }] } }, " ", { p: [19, 6, 479], t: 7, e: "div", a: { "class": "photo-caption" }, f: [{ t: 2, r: "caption", p: [19, 33, 506] }, " ", { p: [19, 47, 520], t: 7, e: "span", a: { "class": "photo-credit" }, f: ["Photograph: ", { t: 2, r: "credit", p: [19, 86, 559] }] }] }] }], n: 50, r: "photourl", p: [15, 5, 399] }, " ", { t: 4, f: [{ p: [24, 6, 639], t: 7, e: "div", a: { "class": "description" }, f: [{ t: 3, r: "description", p: [24, 31, 664] }] }], n: 50, r: "description", p: [23, 5, 613] }] }] }], n: 52, r: "timelineData.sheets.content", p: [7, 3, 105] }] }] }] },
	css: ".gia,p{font-size:16px}.gia{font-family:'Guardian Text Egyptian Web';line-height:24px;margin:0;padding:10px;color:#333;height:auto;overflow:hidden}.headline,a{color:#005689}div{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}p{font-family:'Guardian Sans Web';font-weight:300;line-height:1.5}a{text-decoration:none}.timeline-container{position:relative;margin:0 auto 25px;padding:5px 0 0}.header-content{padding:5px 0 20px}.headline{font-family:'Guardian Egyptian Web';font-size:24px;font-weight:900;line-height:28px;padding:0 0 10px}.event .date,.source-content{font-family:'Guardian Text Sans Web'}.event .date .year,.timeline-container strong{font-weight:700}.timeline-content{height:500px;overflow-x:hidden;overflow-y:auto}.event{padding:0}.content{position:relative;display:inline-block;width:70%;min-height:70px;padding:15px;border-left:4px solid #4dc6dd}.content:before{position:absolute;top:18px;left:-9px;width:10px;height:10px;content:'';border:2px solid #fff;-webkit-border-radius:20px;border-radius:20px;background-color:#333}.content img{width:100%}.event .date{font-size:14px;line-height:18px;display:inline-block;width:25%;padding:15px 10px 0 0;vertical-align:top;text-transform:uppercase}.event .headline{font-size:16px;line-height:22px;padding:0}.event .description{font-size:14px;line-height:22px}.photo,.source-content{font-size:13px;line-height:18px}.source-content{margin:10px 0 0;padding:5px 0 0;border-top:1px solid #333}.photo{font-family:\"Guardian Text Sans Web\",\"Helvetica Neue\",Helvetica,Arial,\"Lucida Grande\",sans-serif;color:#767676;overflow:auto;padding-bottom:10px}"
},
    component = {},
    __prop__,
    __export__;
(function () {
	var __dependencies__ = {
		'toolkit/ajax/get': __import0__
	};

	var require = function require(path) {
		if (__dependencies__.hasOwnProperty(path)) {
			return __dependencies__[path];
		}

		throw new Error('Could not find required module "' + path + '"');
	};

	var get = require('toolkit/ajax/get');

	component.exports = {
		oninit: function oninit() {
			var _this = this;

			var url = this.get('url');

			if (url) {
				get(url).then(String).then(JSON.parse).then(function (data) {
					_this.set('timelineData', data);
				});
			}
		},

		data: {
			log: function log() {
				console.log(arguments);
			}
		}
	};

	if (babelHelpers.typeof(component.exports) === "object") {
		for (__prop__ in component.exports) {
			if (component.exports.hasOwnProperty(__prop__)) {
				__options__[__prop__] = component.exports[__prop__];
			}
		}
	}

	__export__ = Ractive.extend(__options__);
})();

export default __export__;
//# sourceMappingURL=index.js.map
