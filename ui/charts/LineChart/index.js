import Ractive from 'ractive';
import __import0__ from 'toolkit/maths/linearScale';
var __options__ = {
	template: { v: 3, t: [{ p: [1, 1, 0], t: 7, e: "div", a: { "class": "chart" }, f: [{ p: [3, 2, 22], t: 7, e: "svg", a: { width: "100%", height: "100%", xmlns: "http://www.w3.org/2000/svg" }, f: [{ p: [5, 2, 92], t: 7, e: "polyline", a: { points: [{ t: 2, r: "coords", p: [5, 20, 110] }], stroke: "black", fill: "none" } }, " ", { t: 4, f: [{ p: [8, 4, 178], t: 7, e: "circle", a: { cx: [{ t: 2, x: { r: ["xScale", "x"], s: "_0(_1)" }, p: [8, 16, 190] }], cy: [{ t: 2, x: { r: ["yScale", "y"], s: "_0(_1)" }, p: [8, 39, 213] }], r: "5" } }], n: 52, r: "lineData", p: [7, 3, 154] }] }] }] },
	css: ".chart{width:500px;height:500px;background-color:#eee}"
},
    component = {},
    __prop__,
    __export__;
(function () {
	var __dependencies__ = {
		'toolkit/maths/linearScale': __import0__
	};

	var require = function require(path) {
		if (__dependencies__.hasOwnProperty(path)) {
			return __dependencies__[path];
		}

		throw new Error('Could not find required module "' + path + '"');
	};

	var linearScale = require('toolkit/maths/linearScale');

	component.exports = {
		computed: {
			minX: function minX() {
				return this.get('lineData').reduce(function (min, point) {
					return Math.min(min, point.x);
				}, Infinity);
			},
			maxX: function maxX() {
				return this.get('lineData').reduce(function (max, point) {
					return Math.max(max, point.x);
				}, -Infinity);
			},
			minY: function minY() {
				return this.get('lineData').reduce(function (min, point) {
					return Math.min(min, point.y);
				}, Infinity);
			},
			maxY: function maxY() {
				return this.get('lineData').reduce(function (max, point) {
					return Math.max(max, point.y);
				}, -Infinity);
			},
			xScale: function xScale() {
				return linearScale([this.get('minX'), this.get('maxX')], [0, 500]);
			},
			yScale: function yScale() {
				return linearScale([this.get('minY'), this.get('maxY')], [0, 500]);
			},
			coords: function coords() {
				var xScale = this.get('xScale');
				var yScale = this.get('yScale');

				return this.get('lineData').map(function (point) {
					return xScale(point.x) + ',' + yScale(point.y);
				}).join(' ');
			}
		},
		data: function data() {
			return {
				lineData: [{ "x": 2005, "y": 10 }, { "x": 2006, "y": 11 }, { "x": 2007, "y": 62 }, { "x": 2008, "y": 86 }, { "x": 2009, "y": 10 }, { "x": 2010, "y": 45 }, { "x": 2011, "y": 65 }, { "x": 2012, "y": 45 }, { "x": 2013, "y": 50 }, { "x": 2014, "y": 12 }, { "x": 2015, "y": 23 }]
			};
		}
	};

	if (babelHelpers.typeof(component.exports) === "object") {
		for (__prop__ in component.exports) {
			if (component.exports.hasOwnProperty(__prop__)) {
				__options__[__prop__] = component.exports[__prop__];
			}
		}
	}

	__export__ = Ractive.extend(__options__);
})();

export default __export__;
//# sourceMappingURL=index.js.map
