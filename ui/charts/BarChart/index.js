import Ractive from 'ractive';
import __import0__ from 'toolkit/maths/linearScale';
var __options__ = {
	template: { v: 3, t: [{ p: [1, 1, 0], t: 7, e: "div", a: { "class": "wrapper" }, f: [" ", { p: [3, 3, 68], t: 7, e: "svg", a: { "class": "outer" }, f: [{ p: [4, 4, 91], t: 7, e: "svg", a: { "class": "plot", x: [{ t: 2, r: "padding.3", p: [4, 25, 112] }], y: [{ t: 2, r: "padding.0", p: [4, 44, 131] }], width: [{ t: 2, r: "plotWidth", p: [4, 67, 154] }], height: [{ t: 2, r: "plotHeight", p: [4, 90, 177] }] }, f: [{ t: 4, f: [{ p: [6, 6, 223], t: 7, e: "g", a: { transform: ["translate(", { t: 2, x: { r: ["xScale", "i"], s: "_0(_1)" }, p: [6, 30, 247] }, ",0)"] }, f: [{ p: [7, 7, 272], t: 7, e: "rect", a: { x: "5", y: [{ t: 2, x: { r: ["plotHeight", "yScale", "value"], s: "_0-_1(_2)" }, p: [9, 11, 301] }], width: [{ t: 2, x: { r: ["barWidth"], s: "_0-10" }, p: [10, 15, 347] }], height: [{ t: 2, x: { r: ["yScale", "value"], s: "_0(_1)" }, p: [11, 16, 379] }], fill: "#52c6d8" } }, " ", { p: [15, 7, 436], t: 7, e: "text", a: { "class": "label", x: [{ t: 2, x: { r: ["barWidth"], s: "_0/2" }, p: [15, 30, 459] }], y: [{ t: 2, x: { r: ["plotHeight"], s: "_0+20" }, p: [15, 49, 478] }] }, f: [{ t: 2, r: "label", p: [15, 70, 499] }] }, " ", { p: [16, 7, 522], t: 7, e: "text", a: { "class": "value", x: [{ t: 2, x: { r: ["barWidth"], s: "_0/2" }, p: [16, 30, 545] }], y: [{ t: 2, x: { r: ["plotHeight", "yScale", "value"], s: "_0+30-_1(_2)" }, p: [16, 49, 564] }] }, f: [{ t: 2, r: "value", p: [16, 86, 601] }] }] }], n: 52, i: "i", r: "points", p: [5, 5, 198] }] }] }, " "] }] },
	css: ".wrapper{position:relative;width:100%;height:0;padding:0 0 50%;font-family:'Guardian Text Sans Web'}svg{position:absolute;width:100%;height:100%}.plot{overflow:visible}text{fill:#fff;text-anchor:middle}.label{fill:#333}"
},
    component = {},
    __prop__,
    __export__;
(function () {
	var __dependencies__ = {
		'toolkit/maths/linearScale': __import0__
	};

	var require = function require(path) {
		if (__dependencies__.hasOwnProperty(path)) {
			return __dependencies__[path];
		}

		throw new Error('Could not find required module "' + path + '"');
	};

	var linearScale = require('toolkit/maths/linearScale');

	component.exports = {
		data: function data() {
			return {
				containerWidth: 100,
				containerHeight: 100,
				padding: [2, 2, 30, 2]
			};
		},

		computed: {
			plotWidth: '${containerWidth} - (${padding}[1] + ${padding[3]})',
			plotHeight: '${containerHeight} - (${padding}[0] + ${padding[2]})',

			xScale: function xScale() {
				return linearScale([0, this.get('points').length], [0, this.get('plotWidth')]);
			},
			yScale: function yScale() {
				return linearScale([0, this.get('max')], [0, this.get('plotHeight')]);
			},
			barWidth: function barWidth() {
				return this.get('xScale')(1);
			},
			max: function max() {
				return this.get('points').reduce(function (max, point) {
					return Math.max(max, point.value);
				}, -Infinity);
			},
			min: function min() {
				return this.get('points').reduce(function (min, point) {
					return Math.min(min, point.value);
				}, Infinity);
			}
		},

		onrender: function onrender() {
			var _this = this;

			this.container = this.find('.wrapper');

			var resize = function resize() {
				return _this.resize();
			};

			this.on('unrender', function () {
				return window.removeEventListener('resize', resize, false);
			});
			window.addEventListener('resize', resize, false);

			this.resize();
		},
		resize: function resize() {
			this.set({
				containerWidth: this.container.offsetWidth,
				containerHeight: this.container.offsetHeight
			});
		}
	};

	if (babelHelpers.typeof(component.exports) === "object") {
		for (__prop__ in component.exports) {
			if (component.exports.hasOwnProperty(__prop__)) {
				__options__[__prop__] = component.exports[__prop__];
			}
		}
	}

	__export__ = Ractive.extend(__options__);
})();

export default __export__;
//# sourceMappingURL=index.js.map
