var gobble = require( 'gobble' );

module.exports = gobble([
	gobble( 'src/ui' )
		.transform( 'ractive', { type: 'es6' })
		.transform( 'babel' )
]);
