import { doc } from '../core/environment';

/*global Element, navigator */
// Ripped from screenfull.js
// https://github.com/sindresorhus/screenfull.js/blob/gh-pages/license
const keyboardAllowed = typeof Element !== 'undefined' && 'ALLOW_KEYBOARD_INPUT' in Element;

const fn = (function () {
	if ( !doc ) return false;

	var val;
	var valLength;

	var fnMap = [
		[
			'requestFullscreen',
			'exitFullscreen',
			'fullscreenElement',
			'fullscreenEnabled',
			'fullscreenchange',
			'fullscreenerror'
		],
		// new WebKit
		[
			'webkitRequestFullscreen',
			'webkitExitFullscreen',
			'webkitFullscreenElement',
			'webkitFullscreenEnabled',
			'webkitfullscreenchange',
			'webkitfullscreenerror'

		],
		// old WebKit (Safari 5.1)
		[
			'webkitRequestFullScreen',
			'webkitCancelFullScreen',
			'webkitCurrentFullScreenElement',
			'webkitCancelFullScreen',
			'webkitfullscreenchange',
			'webkitfullscreenerror'

		],
		[
			'mozRequestFullScreen',
			'mozCancelFullScreen',
			'mozFullScreenElement',
			'mozFullScreenEnabled',
			'mozfullscreenchange',
			'mozfullscreenerror'
		],
		[
			'msRequestFullscreen',
			'msExitFullscreen',
			'msFullscreenElement',
			'msFullscreenEnabled',
			'MSFullscreenChange',
			'MSFullscreenError'
		]
	];

	var i = 0;
	var l = fnMap.length;
	var ret = {};

	for (; i < l; i++) {
		val = fnMap[i];
		if (val && val[1] in doc) {
			for (i = 0, valLength = val.length; i < valLength; i++) {
				ret[fnMap[0][i]] = val[i];
			}
			return ret;
		}
	}

	return false;
})();

let screenfull;

if (!fn) {
	screenfull = false;
} else {
	screenfull = {
		request (elem) {
			const request = fn.requestFullscreen;

			elem = elem || doc.documentElement;

			// Work around Safari 5.1 bug: reports support for
			// keyboard in fullscreen even though it doesn't.
			// Browser sniffing, since the alternative with
			// setTimeout is even worse.
			if (/5\.1[\.\d]* Safari/.test(navigator.userAgent)) {
				elem[request]();
			} else {
				elem[request](keyboardAllowed && Element.ALLOW_KEYBOARD_INPUT);
			}
		},
		exit () {
			doc[fn.exitFullscreen]();
		},
		toggle (elem) {
			if (this.isFullscreen) {
				this.exit();
			} else {
				this.request(elem);
			}
		},
		raw: fn
	};

	Object.defineProperties( screenfull, {
		isFullscreen: {
			get () {
				return !!doc[fn.fullscreenElement];
			}
		},
		element: {
			enumerable: true,
			get () {
				return doc[fn.fullscreenElement];
			}
		},
		enabled: {
			enumerable: true,
			get () {
				// Coerce to boolean in case of old WebKit
				return !!doc[fn.fullscreenEnabled];
			}
		}
	});
}

export default screenfull;
