# dom

## addClass

```js
import addClass from 'utils/dom/addClass';

addClass( document.body, 'gia' );
```

## forceRedraw

Note: this *will* fuck things up if you're not careful. Use only when you absolutely have to.

```js
import forceRedraw from 'utils/dom/forceRedraw';

doSomethingInvolvingFiltersAndStuff(); // causes rendering glitches
forceRedraw(); // fixes them
```

## query

Shorthand for `document.querySelector`:

```js
import $ from 'utils/dom/query';

let main = $( 'main' );
```

## queryAll

Shorthand for `document.querySelectorAll`:

```js
import $$ from 'utils/dom/queryAll';

let listItems = $$( 'li.gia' );
```

## removeClass

```js
import removeClass from 'utils/dom/removeClass';

removeClass( document.body, 'gia' );
```