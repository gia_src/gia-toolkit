import { doc } from '../core/environment';

export default function forceRedraw () {
	doc.body.style.display = 'inline-block';
	doc.body.offsetHeight; // no need to store this anywhere, the reference is enough
	doc.body.style.display = 'block';
}
