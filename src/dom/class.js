const trim = str => str.trim();

export function addClass ( node, className ) {
	var classNames;

	if ( node.classList && node.classList.add ) {
		node.classList.add( className );
	}

	classNames = node.className.split( ' ' ).map( trim ).filter( Boolean );

	if ( !~classNames.indexOf( className ) ) {
		classNames.push( className );
		node.className = classNames.join( ' ' );
	}
}

export function removeClass ( node, className ) {
	var classNames, index, dirty;

	if ( node.classList && node.classList.remove ) {
		node.classList.remove( className );
	}

	classNames = node.className.split( ' ' ).map( trim ).filter( Boolean );

	while ( ~( index = classNames.indexOf( className ) ) ) {
		classNames.splice( index, 1 );
		dirty = true;
	}

	if ( dirty ) {
		node.className = classNames.join( ' ' );
	}
}
