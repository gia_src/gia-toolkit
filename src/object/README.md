# object

## assign

Assigns the properties of one or more sources to a target object:

```js
import assign from 'utils/object/assign';

let a = { foo: 'bar' };
let b = { baz: 'qux' };

let c = assign( {}, a, b ); // { foo: 'bar', baz: 'qux' }

assign( a, b );
a.baz; // 'qux'
```


## deepEqual

```js
import deepEqual from 'utils/object/deepEqual';

deepEqual( 42, 42 ); // true

let a = { foo: 'bar' };
deepEqual( a, a ); // true

let b = { foo: 'bar' };
a === b; // false
deepEqual( a, b ); // true
```

Note: do not use this with cyclical structures! There is no detection mechanism.