var assign = Object.assign || function assign ( target ) {
	var i, source;

	for ( i = 1; i < arguments.length; i += 1 ) {
		source = arguments[i];
		Object.keys( source ).forEach( key => target[ key ] = source[ key ] );
	}

	return target;
};

export default assign;