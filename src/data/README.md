# data

## csvToArray

Takes a CSV string, and returns an array of arrays (an array of rows, each of which is an array of cells). Used internally by csvToJson.js.

```js
import csvToArray from 'utils/data/csvToArray';

rows = csvToArray( someCsvData ); // optionally, pass a delimiter as second argument
```


## csvToJson

Takes a CSV string, and returns an array of objects. The first non-empty row is used as headers.

```js
import csvToJson from 'utils/data/csvToJson';

records = csvToJson( someCsvData ); // optionally, pass a delimiter as second argument
```


## reindex

Reindexes an array of objects by a common field.

```js
import reindex from 'utils/data/reindex';

countries = [
	{ code: 'AFG', name: 'Afghanistan'    },
	{ code: 'ETH', name: 'Ethiopia'       },
	{ code: 'FRA', name: 'France'         },
	{ code: 'GBR', name: 'United Kingdom' }
];

countryByCode = reindex( countries, 'code' );

console.log( countryByCode.ETH.name ); // -> 'Ethiopia'
```