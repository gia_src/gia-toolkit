import { isArray } from '../core/types';

export default function reindex ( array, idField ) {
	var obj = {}, i, record, id;

	if ( !isArray( array ) ) {
		throw new Error( 'Cannot reindex a non-array!' );
	}

	if ( !idField ) {
		throw new Error( 'You must specify an ID field' );
	}

	i = array.length;
	while ( i-- ) {
		record = array[i];
		if ( record.hasOwnProperty( idField ) ) {
			id = record[ idField ];

			if ( obj.hasOwnProperty( id ) ) {
				throw new Error( `Duplicate ID (${id})` );
			}

			obj[ record[ idField ] ] = record;
		}
	}

	return obj;
}
