# compatibility

## addWheelListener

A sane cross-browser mousewheel event listener:

```js
import addWheelListener from 'utils/compatibility/addWheelListener';

addWheelListener( domNode, event => {
	scrollSomethingBy( event.deltaY );
});
```