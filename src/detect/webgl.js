import { doc, win } from '../core/environment';

const webgl = (function () {
	try {
		const canvas = doc.createElement( 'canvas' );
		return !! win.WebGLRenderingContext && ( canvas.getContext( 'webgl' ) || canvas.getContext( 'experimental-webgl' ) );
	} catch ( e ) {
		return false;
	}
})();

export default win && !~win.navigator.userAgent.toLowerCase().indexOf( 'msie' ) && webgl;
