# image

## loadImage

```js
import loadImage from 'utils/image/loadImage';

loadImage( 'path/to/image.jpg' )
	.then( img => {
		el.appendChild( img );
		el.opacity = 1; // fade in
	})
	.catch( err => {
		console.error( 'failed to load image' );
	});
```