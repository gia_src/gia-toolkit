import { isArray } from '../core/types';
import { doc, win } from '../core/environment';
import Promise from '../core/promise';

const head = doc && doc.getElementsByTagName( 'head' )[0];

export default function jsonp ( url, params, callbackParam ) {
	return new Promise( ( fulfil, reject ) => {
		if ( typeof params === 'string' ) {
			callbackParam = params;
			params = {};
		} else {
			params = params || {};
			callbackParam = callbackParam || 'jsonp_callback';
		}

		const callbackName = params[ callbackParam ] || 'load_jsonp_' + Math.round( Math.random() * 1000000 );
		if ( !params[ callbackParam ] ) {
			params[ callbackParam ] = callbackName;
		}

		const query = serializeParams( params );

		win[ callbackName ] = fulfil;

		const script = doc.createElement( 'script' );
		script.src = url + '?' + query;

		script.onload = function () {
			script.parentNode.removeChild( script );
			delete win[ callbackName ];
		};

		script.onerror = reject;

		head.appendChild( script );
	});
}

function serializeParams ( params ) {
	return Object.keys( params ).map( key => {
		const value = params[ key ];

		if ( isArray( value ) ) {
			return value.map( value => {
				return serializePair( key, value );
			}).join( '&' );
		}

		return serializePair( key, value );
	}).join( '&' );
}

function serializePair ( key, value ) {
	return key + '=' + encodeURIComponent( value );
}
