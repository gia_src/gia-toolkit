import { win } from '../core/environment';
import Promise from '../core/promise';

const XMLHttpRequest = win && win.XMLHttpRequest;
const XDomainRequest = win && win.XDomainRequest;

export default function get ( url, options = {} ) {
	return new Promise( ( fulfil, reject ) => {
		const xhr = new XMLHttpRequest();

		xhr.open( 'GET', url );

		if ( options.responseType ) {
			xhr.responseType = options.responseType;
		}

		xhr.onload = function () {
			if ( /^[^2]/.test( xhr.status ) ) {
				console.log( xhr );
				reject( new Error( `Request failed: ${xhr.status}` ) );
			} else {
				fulfil( options.responseType ? xhr.response : xhr.responseText );
			}
		};

		xhr.onerror = reject;

		xhr.send();
	});
}
