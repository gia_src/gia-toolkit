# ajax

## fetch

A polyfill for an upcoming spec:

```js
import fetch from 'utils/ajax/fetch';

fetch( 'some-data.json' )
	.then( response => response.json() ) // or response.text(), for non-JSON
	.then( data => {
		doSomethingWith( data );
	})
	.catch( err => {
		// oh noes!
		console.error( err );
	});
```

See [github.com/github/fetch](https://github.com/github/fetch) for complete documentation.


## get

Does a similar job to fetch, but rather more simply as it doesn't attempt to support a spec. Use it if you don't need the full capabilities of fetch:

```js
import get from 'utils/ajax/get';

get( 'some-data.json' )
	.then( JSON.parse )
	.then( data => {
		doSomethingWith( data );
	})
	.catch( err => {
		console.error( err );
	});
```


## jsonp

Fetches a JSONP resource. In many situations it will 'just work', but you may wish to set the callback name explicitly (particularly if, for example, you're serving JSONP from S3):

```js
import jsonp from 'utils/ajax/jsonp'; // yeah, okay, JSONP isn't AJAX. Sue me

// this will set everything up automatically - it will append
// something like ?jsonp_callback=load_jsonp_123456 to the URL
jsonp( `http://some-api-provider.com/endpoint/${id}` )
	.then( data => {
		doSomethingWith( data );
	})
	.catch( err => {
		console.error( err );
	});

// Some API providers use a different parameter, e.g. callback
// instead of jsonp_callback
jsonp( `http://another-api-provider.com/endpoint/${id}`, 'callback' )
	.then( data => {
		doSomethingWith( data );
	})
	.catch( err => {
		console.error( err );
	});

// If you're grabbing data from S3, the callback name must match the
// name of the function that the file calls
jsonp( `http://interactive.guim.co.uk/2015/02/test/some-data.js`, {
	jsonp_callback: 'GIA_SETDATA'
})
	.then( data => {
		doSomethingWith( data );
	})
	.catch( err => {
		console.error( err );
	});
```