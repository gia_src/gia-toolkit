import { isArray, isObject, isNumeric } from '../core/types';

function interpolateNumber ( from, to ) {
	if ( !isNumeric( from ) || !isNumeric( to ) ) return null;

	from = +from;
	to = +to;

	const delta = to - from;

	if ( !delta ) return () => from;

	return t => from + ( t * delta );
}

function interpolateArray ( from, to ) {
	if ( !isArray( from ) || !isArray( to ) ) return null;

	let intermediate = [];
	let interpolators = [];

	const len = Math.min( from.length, to.length );
	let i = len;

	while ( i-- ) {
		interpolators[i] = interpolate( from[i], to[i] );
	}

	// surplus values - don't interpolate, but don't exclude them either
	for ( i = len; i < from.length; i += 1 ) {
		intermediate[i] = from[i];
	}

	for ( i = len; i < to.length; i += 1 ) {
		intermediate[i] = to[i];
	}

	return function ( t ) {
		let i = len;

		while ( i-- ) {
			intermediate[i] = interpolators[i]( t );
		}

		return intermediate;
	};
}

function interpolateObject ( from, to ) {
	if ( !isObject( from ) || !isObject( to ) ) return null;

	let properties = [];
	let intermediate = {};
	let interpolators = {};

	let prop;

	for ( prop in from ) {
		if ( hasOwnProperty.call( from, prop ) ) {
			if ( hasOwnProperty.call( to, prop ) ) {
				properties.push( prop );
				interpolators[ prop ] = interpolate( from[ prop ], to[ prop ] );
			}

			else {
				intermediate[ prop ] = from[ prop ];
			}
		}
	}

	for ( prop in to ) {
		if ( hasOwnProperty.call( to, prop ) && !hasOwnProperty.call( from, prop ) ) {
			intermediate[ prop ] = to[ prop ];
		}
	}

	const len = properties.length;

	return function ( t ) {
		let i = len;
		let prop;

		while ( i-- ) {
			prop = properties[i];
			intermediate[ prop ] = interpolators[ prop ]( t );
		}

		return intermediate;
	};
}

function snap ( to ) {
	return () => to;
}

export default function interpolate ( from, to ) {
	if ( from === to ) {
		return snap( to );
	}

	return interpolateNumber( from, to ) ||
	       interpolateArray( from, to ) ||
	       interpolateObject( from, to ) ||
	       snap( to );
}
