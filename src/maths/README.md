# maths

## easing

A collection of Robert Penner's easing equations, many of which you can preview [here](http://easings.net/). Some of them are kind of sucky but most are rather fun.

```js
import { bounceOut } from 'utils/maths/easing';
import tween from 'utils/animate/tween';

let ball = document.querySelector( 'circle' );

tween( -500, 0, {
	duration: 1000,
	easing: bounceOut,
	step: val => {
		ball.setAttribute( 'cy', val );
	}
})
```

The complete list is as follows:

* backInOut
* backIn
* backOut
* bounceInOut
* bounceIn
* bounceOut
* circInOut
* circIn
* circOut
* cubicInOut
* cubicIn
* cubicOut
* elasticInOut
* elasticIn
* elasticOut
* expoInOut
* expoIn
* expoOut
* linear
* quadInOut
* quadIn
* quadOut
* quarticInOut
* quarticIn
* quarticOut
* qinticInOut
* qinticIn
* qinticOut
* sineInOut
* sineIn
* sineOut


## interpolate

Creates a function that interpolates between two values. Those values must be numbers, or arrays or objects that contain numbers (or other arrays/objects containing numbers...) - interpolating e.g. matrix transform strings or SVG paths is not yet supported.

```js
import interpolate from 'utils/maths/interpolate';

let interpolator = interpolate( 50, 75 );

interpolator( 0 ); // 50
interpolator( 0.2 ); // 55
interpolator( 0.8 ); // 70
interpolator( 1 ); // 75
```


## linearScale

Creates a function that maps a value from a given domain to a given range:

```js
import linearScale from 'utils/maths/linearScale';

let x1 = 100;
let x2 = 400;

let xScale = linearScale([ 0, 100 ], [ x1, x2 ]);

xScale( 0 ); // 100
xScale( 50 ); // 250
xScale( 100 ); // 400
```


## pythag

The Pythagorean theorem:

```js
import pythag from 'utils/maths/pythag';

let a = { x: 10, y: 10 };
let b = { x: 40, y: 50 };

let distance = pythag( b.x - a.x, b.y - a.y ); // 50
```