/*global Audio, console */
import { isObject } from '../core/types';
import Promise from '../core/promise';

export default function loadAudio ( sources ) {
	if ( typeof sources === 'string' ) {
		sources = {
			mp3: `${sources}.mp3`,
			ogg: `${sources}.ogg`
		};
	}

	if ( !isObject( sources ) ) {
		throw new Error( 'loadAudio expects a `sources` object with one or more of the following properties: `mp3`, `ogg`' );
	}

	return new Promise( ( fulfil, reject ) => {
		let audio = new Audio();

		if ( !audio.canPlayType ) {
			reject( 'Audio does not appear to be supported' );
		}

		audio.onerror = e => {
			console.warn( 'error loading audio', sources );
			reject( e );
		};

		audio.addEventListener( 'canplaythrough', () => {
			audio.pause();
			fulfil( audio );
		});

		if ( sources.mp3 && ( audio.canPlayType( 'audio/mpeg' ) !== '' ) ) {
			audio.setAttribute( 'src', sources.mp3 );
		} else if ( sources.ogg && ( audio.canPlayType( 'audio/ogg; codecs="vorbis"' ) !== '' ) ) {
			audio.setAttribute( 'src', sources.ogg );
		}

		else {
			reject( 'No valid sources found' );
		}
	});
}