# audio

## loadAudio

This may not be entirely fool-proof across all browsers. Use it as a preloading mechanism, but maybe don't rely on it to actually load stuff. Patches welcome...

```js
import loadAudio from 'utils/audio/loadAudio';

// if you have an mp3 and an ogg file at
// http://some-url.com/files/audio.mp3 and
// http://some-url.com/files/audio.ogg, you
// can pass a string
loadAudio( 'http://some-url.com/files/audio' );

// otherwise be explicit about your sources. Only
// mp3 and ogg are supported
loadAudio({
	mp3: 'http://some-url.com/files/audio.mp3',
	ogg: 'http://some-url.com/files/audio.ogg'
});

// in either case, a promise is returned that fulfils with
// the audio node, or rejects with the error. But like I
// say, it's probably better that you just use it to
// kick off a preload
```