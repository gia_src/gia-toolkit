# core

## debug

This bypasses the promise try-catch mechanism, allowing you to see stack traces from errors thrown inside a promise chain:

```js
import debug from 'utils/core/debug';

doSomethingThatReturnsAPromise()
	.then( doSomethingElse )
	.then( andSomethingElse )
	.then( andAnotherThingAfterThat )
	.catch( debug );
```

## history

Convenience layer between you and the HTML5 History API. It will work everywhere, though on IE9 and below (and Opera Mini, for anyone who's keeping score) using the browser's forward and back buttons won't move you through the stack.

```js
import history from 'utils/core/history';

// let's say we're creating a stepper... we first need to
// add a state object to the browser history. The URL
// is optional, and won't have an effect in IE9
history.replace({ step: 0 }, 'steps/0' );

// when we advance to the next step, we push a state object
// and update the URL. Note that the URL is relative to the
// current URL, i.e. we go to `steps/1` then `steps/2` etc
history.push({ step: 1 }, '1' );
history.push({ step: 2 }, '2' );

// if the user uses the browser's (or phone's, in the case
// of Android) back/forward buttons, we can capture the state
history.onstate( state => {
	goto( state.step );
});

// we can also trigger moves programmatically
history.back();
history.forward();
history.go( -2 );
```

## promise

A polyfill for ES6 promises, for browsers (cough *Safari* cough) that don't support them.

```js
import Promise from 'utis/core/promise';

let waitASecond = new Promise( ( fulfil, reject ) => {
	setTimeout( fulfil, 1000 );
});

waitASecond.then( () => {
	console.log( 'a second has elapsed' );
});
```

## storage

Convenience layer between you and `localStorage`. If `localStorage` isn't supported (which is sometimes the case in even modern versions of Firefox, if the user has certain privacy settings), you can still use it (values will be stored in memory) but data will not be persisted.

Most browsers only allow 5Mb of storage per domain, and we may be sharing our quota with other stuff on theguardian.com, so don't assume data will be persisted. Use it sparingly, and plan accordingly.

`localStorage` only deals with strings. This module extends that to anything that can be serialised with `JSON.stringify()`.

```js
import storage from 'utils/core/storage';

if ( !storage.supported ) {
	console.log( 'Scores will not be persisted' );
}

let highscore = storage.get( 'GIA_HIGHSCORE' );

// later...
storage.set( 'GIA_HIGHSCORE', 42 );

// items can be removed from storage
storage.remove( 'GIA_HIGHSCORE' );
```

## types

```js
import { isArray, isObject, isNumeric } from 'utils/core/types';

let arr = [ 1, 2, 3 ];
typeof arr; // 'object'
isArray([ 1, 2, 3 ]); // true

typeof null; // 'object'
isObject( null ); // false

let obj = { foo: 'bar' };
typeof obj; // 'object'
isObject( obj ); // true

typeof NaN; // 'number';
isNumeric( NaN ); // false

typeof '42'; // 'string'
isNumeric( '42' ); true

typeof 42; // 'number'
isNumeric( 42 ); true
```