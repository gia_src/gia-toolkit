import { win } from './environment';

var history;

if ( win && win.history && typeof win.history.pushState === 'function' && typeof win.history.replaceState === 'function' ) {
	history = {
		push ( state, url ) {
			win.history.pushState( state, 'x', url );
		},

		replace ( state, url ) {
			win.history.replaceState( state, 'x', url );
		},

		back () {
			win.history.back();
		},

		forward () {
			win.history.forward();
		},

		go ( n ) {
			win.history.go( n );
		},

		onstate ( cb ) {
			win.addEventListener( 'popstate', e => cb( e.state ), false );
		}
	};
}

else {
	let stack = [ null ];
	let callbacks = [];
	let index = 0;

	history = {
		push ( state ) {
			index += 1;
			this.replace( state );
		},

		replace ( state ) {
			stack[ index ] = state;
			stack.length = index + 1;
		},

		back () {
			history.go( -1 );
		},

		forward () {
			history.go( 1 );
		},

		go ( n ) {
			index += n;
			callbacks.forEach( cb => {
				cb( stack[ index ] );
			});
		},

		onstate ( cb ) {
			callbacks.push( cb );
		}
	};
}

export default history;
