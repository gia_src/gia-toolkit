const toString = Object.prototype.toString;

export function isArray ( thing ) {
	return toString.call( thing ) === '[object Array]';
}

export function isObject ( thing ) {
	return ( thing && toString.call( thing ) === '[object Object]' );
}

export function isNumeric ( thing ) {
	return !isNaN( parseFloat( thing ) ) && isFinite( thing );
}