export default function bucket ( array, numBuckets ) {
	let buckets = new Array( numBuckets );
	let bucketSize = array.length / numBuckets;

	let start;
	let end;

	for ( let i = 0; i < numBuckets; i += 1 ) {
		start = ~~( i * bucketSize );
		end = ~~( ( i + 1 ) * bucketSize );

		buckets[i] = array.slice( start, end );
	}

	return buckets;
}
