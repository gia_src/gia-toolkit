# array

## find

```js
import find from 'utils/array/find';

// given an array of objects...
let arr = [
	{ id: 1, name: 'alice' },
	{ id: 2, name: 'bob' },
	{ id: 3, name: 'charles' }
];

// we can find the first one that satisfies
// a particular condition
let item = find( arr, x => x.id === 2 );
// -> { id: 2, name: 'bob' }
```

## pickRandom

```js
import pickRandom from 'utils/array/pickRandom';

let dwarfs = [ 'Doc', 'Grumpy', 'Happy', 'Sleepy', 'Bashful', 'Sneezy', 'Dopey' ];
let dwarf = pickRandom( dwarves );
```

## range

```js
import range from 'utils/array/range';

let arr = range( 0, 10 ); // [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
```

## shuffle

```js
import shuffle from 'utils/array/shuffle';

let deckOfCards = [
	{ suit: 'spade', rank, 1 },
	{ suit: 'spade', rank, 2 },
	{ suit: 'spade', rank, 3 },
	// ...
];

// note: the original array is mutated
shuffle( deckOfCards );

// it's also returned, so you can do things like
// this if you don't want to mutate the original
const DECK_OF_CARDS = [ /* ... */ ];
let shuffled = shuffle( DECK_OF_CARDS.slice() );
```