# animate

## tween

Tweens two values. These values can be (or contain) arrays (of the same length, ideally...) or objects (with the same properties):

```js
import tween from 'utils/animate/tween';
import { elasticOut } from 'utils/maths/easing';

var animation = tween( 0, 100, {
	duration: 800,           // milliseconds
	easing: elasticOut,      // see utils/maths/easing
	step: val => {
		doSomethingWith( val );
	}
});

// the return animation is a promise...
animation.then( doAnotherAnimation );

// ...but also has a stop method. Calling it will
// prevent the promise from fulfilling (i.e. the
// `then` handler is never called)
setTimeout( animation.stop, 500 );
```