# timing

## now

Returns the current time, relative to nothing whatsoever. Use it for calculating intervals. In modern browsers, it will use the high-resolution `window.performance.now()`, falling back to `Date.now()` and finally `new Date().getTime()`.

```js
import now from 'utils/timing/now';

let start = now();
doSomethingComputationallyExpensive();
let elapsed = now() - start;

if ( elapsed > 100 ) {
	// looks like we're in a slow browser
	disableFancyEffectsAndWhatnot();
}
```


## rAF

Polyfill for `requestAnimationFrame`, except that it doesn't register itself as a global.

```js
import rAF from 'utils/timing/rAF';

let i = 0;

let loop = () => {
	rAF( loop );
	console.log( 'the loop has run %s times', ++i );
};

rAF( loop );
```


## throttle

Prevent a function from being called more than once every *n* milliseconds:

```js
import throttle from 'utils/timing/throttle';

// if interval is unspecified, defaults to 250ms
let handleResize = throttle( updateLayout, 500 );

window.addEventListener( 'resize', handleResize, false );
```