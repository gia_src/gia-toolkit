import { win } from '../core/environment';

let now;

if ( win && win.performance && typeof win.performance.now === 'function' ) {
	now = () => win.performance.now();
} else if ( typeof Date.now === 'function' ) {
	now = () => Date.now();
} else {
	now = () => new Date().getTime();
}

export default now;
