import { win } from '../core/environment';

// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

// MIT license

let rAF = win && win.requestAnimationFrame;

if ( !rAF ) {
	const vendors = ['ms', 'moz', 'webkit', 'o'];

	for ( let i = 0; i < vendors.length && !rAF; ++i ) {
		rAF = win && win[ `${vendors[i]}RequestAnimationFrame` ];
	}

	if ( !rAF ) {
		let lastTime = 0;

		rAF = function( callback ) {
			var currTime, timeToCall, id;

			currTime = new Date().getTime();
			timeToCall = Math.max( 0, 16 - ( currTime - lastTime ) );
			id = setTimeout( function() {
				callback( currTime + timeToCall );
			}, timeToCall );

			lastTime = currTime + timeToCall;
			return id;
		};
	}
}

export default rAF;
