# string

## addCommas

```js
import addCommas from 'utils/string/addCommas';

addCommas( 1234 ); // '1,234'
addCommas( 1234567.89 ); // '1,234,567.89'
```

## pad

```js
import pad from 'utils/string/pad';

let day = 2;
let month = 3;
let year = 2015;

let date = `${pad(day,2)}/${pad(month,2)}/${year}`; // '02/03/2015'
```


## slugify

```js
import slugify from 'utils/string/slugify';

slugify( 'Lowercase and hyphens only, please!' );
// 'lowercase-and-hyphens-only-please'
```


## toTitleCase

```js
import toTitleCase from 'utils/string/toTitleCase';

toTitleCase( 'you won\'t believe what happened next!' );
// 'You Won\'t Believe What Happened Next!'
```