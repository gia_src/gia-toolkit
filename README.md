# gia-toolkit

Common utilities and Ractive components for Guardian US interactive projects. See individual folders for examples of usage.

Patches and additions welcome. None of this code will end up in projects unless it's imported, so you can add as much as you like without worrying about project code size.